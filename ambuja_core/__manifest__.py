# -*- coding: utf-8 -*-

{
    'name': "Ambuja Alumni Portal",
    'sequence': 1,
    'summary': """
        Alumni Portal""",

    'description': """
        This module is developed for registration purpose of students.
    """,

    'author': "Modifyed",
    'website': "",
    'category': 'Generic Module',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','mail','product','account','sale_management','hr','sync_smsclient'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'wizard/fees_wizard_view.xml',
        'views/mobilization_source_view.xml',
        'views/student_view.xml',
        'views/enquiry_view.xml',
        'views/account_move_inherit_view.xml',
        'views/batch_view.xml',
        'views/employee_view.xml',
        'views/assessment_view.xml',
        'views/parent_meeting_view.xml',
        'views/industry_visit_view.xml',
        'views/institute_view.xml',
        'views/course_code_view.xml',
        'views/courses_view.xml',
        'views/sectors_view.xml',
        'views/religion_view.xml',
        'data/sequence.xml',
        'views/toolkit_view.xml',
        'views/education_level_view.xml',
        'views/skill_group_view.xml',
        'views/res_config_settings_view.xml',
        'views/company_view.xml',
        'views/scope_of_partnership_view.xml',
        'views/mail_view.xml',
        'views/annual_household_income_view.xml',
        'views/salutation_view.xml',
        'views/fiscal_year_view.xml',
        'views/sms_view.xml',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
