# -*- coding: utf-8 -*-

from odoo import models, fields
from odoo.exceptions import UserError


class Fees(models.TransientModel):
    _name = 'amb.student.fees.wizard'
    _description = 'This Model opeans a wizard and adds fees invoices for a student'

    student_id = fields.Many2one('res.partner', string='Student')
    submit_date = fields.Date(string='Signature Date',
                                 default=fields.Date.today(), required=True)
    fee_paid_by = fields.Selection([
        ('self', 'Self'),
        ('other', 'Other')
    ], string='Fee Paid By',default='self')
    source_funding = fields.Char(string='Source of Funding')

    invoice_line_ids = fields.One2many('amb.student.fees.lines.wizard', 'fees_wizard_id', string='Invoices')

    def submit_invoice(self):
        account_id = self.env['account.account'].search([], limit=1)
        invoices=[]
        res_product = self.env["ir.config_parameter"].sudo().get_param("amb_core.product_id", default=False)
        product = self.env['product.product'].search([('id', '=', int(res_product))])
        if not product:
            raise UserError('Please configure Product in Settings --> Sales --> Student Fees Configuration')
        for rec in self.invoice_line_ids:
            product.lst_price = rec.amount
            invoice_vals = [
                (
                    0,
                    0,
                    {
                        "product_id": product.id,
                        "quantity": 1.0,
                        "account_id": account_id.id,
                        "name": product.name,
                        "price_unit": rec.amount,
                    },
                )
            ]
            fees_move = self.env['account.move'].create({
                'partner_id': self.student_id.id,
                'course_id': self.student_id.course_id.id,
                'batch_name_id': self.student_id.batch_name_id.id,
                's_covered': self.student_id.batch_name_id.s_covered.id,
                'fee_paid_by': self.fee_paid_by,
                'source_funding': self.source_funding,
                'type': 'out_invoice',
                'invoice_date_due': rec.invoice_date_due,
                'invoice_date': self.submit_date,
                'invoice_line_ids': invoice_vals
            })
            invoices.append(fees_move.id)
        if len(invoices) > 0:
            self.student_id.invoice_created = True
            self.student_id.fee_paid_by = fees_move.fee_paid_by
            self.student_id.source_funding = fees_move.source_funding

        tree_view = self.env.ref('ambuja_core.amb_view_invoice_tree').id
        form_view = self.env.ref('ambuja_core.amb_view_move_form').id
        return {
            'name': ('Invoices'),
            'type': 'ir.actions.act_window',
            'res_model': 'account.move',
            'views': [(tree_view, 'tree'),(form_view, 'form')],
            'domain': [('id', 'in', invoices),('type', '=', 'out_invoice')],
        }

class FessLines(models.TransientModel):
    _name = 'amb.student.fees.lines.wizard'
    _description = 'Fees Lines'

    fees_wizard_id = fields.Many2one('amb.student.fees.wizard', string='Wizard ID')
    invoice_date_due = fields.Date(string='Payment Date', required=True)
    amount = fields.Float(string='Amount', required=True)