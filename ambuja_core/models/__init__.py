# -*- coding: utf-8 -*-

from . import res_student
from . import batch
from . import employee
from . import assessment
from . import parent_meeting
from . import enquiry
from . import industry_visit
from . import courses
from . import sectors
from . import religion
from . import education_level
from . import toolkit
from . import institute
from . import skill_group
from . import res_config_settings
from . import scope_of_partnership
from . import mail
from . import annual_household_income
from . import salutation
from . import course_code
from . import account_move
from . import sms
from . import fiscal_year
from . import mobilization_source