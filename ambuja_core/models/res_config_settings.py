# -*- coding: utf-8 -*-

from odoo import api, fields, models

class ResConfigSettings(models.TransientModel):
   _inherit = 'res.config.settings'
   
   product_id = fields.Many2one('product.product', string='Product')

   def set_values(self):
       super(ResConfigSettings, self).set_values()
       param = self.env['ir.config_parameter'].sudo()
       field1 = self.product_id and self.product_id.id or False
       param.set_param('amb_core.product_id', field1)

   def get_values(self):
       res = super(ResConfigSettings, self).get_values()
       res.update(
            product_id = int(self.env['ir.config_parameter'].sudo().get_param('amb_core.product_id')),
        )
       return res