# -*- coding: utf-8 -*-

from odoo import models, fields


class CourseCode(models.Model):
    _name = "amb.course.code"
    _inherit = "mail.thread"
    _description = "Course Code Information"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    s_covered = fields.Many2one('amb.batch.sectors', string='Sector Covered', required=True)
    active = fields.Boolean('Active', default=True)