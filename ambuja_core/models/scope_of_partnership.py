# -*- coding: utf-8 -*-

from odoo import models, fields


class ScopePartnership(models.Model):
    _name = "amb.scope.partnership"
    _inherit = "mail.thread"
    _description = "Sectors Information"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 