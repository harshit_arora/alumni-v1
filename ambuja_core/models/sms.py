# -*- coding: utf-8 -*-

from odoo import models, fields, api

class AmbMail(models.Model):
    _name = 'amb.sms'
    _description = 'Mass SMS'
    _rec_name = 'subject'

    subject = fields.Char(string='Subject')
    batch_name_ids = fields.Many2many('amb.batch', string='Batch')
    group_ids = fields.Many2many('amb.skill.group', string='Skill Group')
    recipient_ids = fields.Many2many('res.partner', string='Recepients')
    s_covered = fields.Many2one('amb.batch.sectors', string='Sector Interested')
    sms_type = fields.Selection([
        ('students', 'Enrolled Students'),
        ('enquiry','Enquiry Followup')
    ], string='SMS Type',default='students')

    @api.onchange('sms_type')
    def _onchange_sms_type(self):
        self.recipient_ids=False
        self.batch_name_ids=False
        self.group_ids=False
        self.s_covered=False

    @api.onchange('s_covered')
    def _onchange_s_covered(self):
        if self.sms_type == 'enquiry':
            enquiry_id=self.env['amb.enquiry'].search([('states','!=',['enrolled','new'])])
            self.recipient_ids=False
            for record in enquiry_id:
                if ((record.s_covered.id == self.s_covered.id) and (record.mobile != False)):
                    ids=record.student_id.id
                    print('====id===',ids)
                    self.write({'recipient_ids':[(4,record.student_id.id)]})

    @api.onchange('batch_name_ids')
    def on_change_batch_name_ids(self):
        student_id = self.env['res.partner'].search([('is_student', '=', True)])
        self.recipient_ids=False
        for record in student_id:
            for batch_id in self.batch_name_ids:
                if ((record.batch_name_id.id == batch_id._origin.id) and (record.mobile != False)):
                    self.write({'recipient_ids': [(4,record.id)]})

    @api.onchange('group_ids')
    def on_change_group_ids(self):
        student_id = self.env['res.partner'].search([('is_student', '=', True)])
        result=[]
        if (self.batch_name_ids !=  False):
            for record in student_id:
                for batch_id in self.batch_name_ids:
                    if ((record.batch_name_id.id == batch_id._origin.id) and (record.mobile != False)):
                        for ids in record.skill_group_ids:
                            for group_id in self.group_ids:
                                if(ids.id == group_id._origin.id):
                                    result.append(record.id)
            self.recipient_ids=result