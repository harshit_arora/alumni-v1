# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
import re


class Enquiry(models.Model):
    _name = 'amb.enquiry'
    _description = 'Enquiry Information'
    _inherit = "mail.thread"
    _rec_name = "name"

    states = fields.Selection([
        ('new', 'New'),
        ('in_progress','In Progress'),
        ('enrolled','Enrolled'),
        ('cancel','Cancel')
    ], string='States',default='new')
    enquiry_date = fields.Date(string='Enquiry Date',
                    default=fields.Date.today(), required=True)
    
    email = fields.Char(string='Email')
    first_name = fields.Char(string='First Name')
    middle_name = fields.Char(string='Middle Name')
    last_name = fields.Char(string='Last Name')
    name = fields.Char(string='Name', required=True)
    gender = fields.Selection([
        ('m', 'Male'),
        ('f', 'Female'),
        ('o', 'Other')
    ], string='Gender', default='m')
    dob = fields.Date(string='Date of Birth', required=True)
    parent_name = fields.Char(string='Parent/Guardian Name', required=True)
    mobile = fields.Char(string='Contact No.',size=10 ,required=True)
    mobile_parent = fields.Char(string='Parent Contact No.', size=10)
    aadhar_no = fields.Char(string='Aadhar Number', size=12)
    cycromatic_test = fields.Boolean(string='Psychromatic Test')
    cycromatic_test_value = fields.Char(string='Psychromatic Test Value')
    education_status = fields.Selection([
        ('pursuing', 'Pursuing'),
        ('pass','Pass Out'),
        ('Drop','Drop Out')
    ], string='Educational Status', default='pass')
    employment_status = fields.Selection([
        ('unemployed', 'Unemployed'),
        ('self','Self Employed'),
        ('job','Job')
    ], string='Employment Status', default='unemployed')
    # mobilized_activity = fields.Selection([
    # ], string='Mobilization Source')
    remarks = fields.Text(string='Remarks')
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', default = lambda self: self._get_default_country())
    mobilized_activity = fields.Many2one('amb.mobilization.source', string='Mobilization Source')
    student_id = fields.Many2one('res.partner', string='Student Name')
    s_covered = fields.Many2one('amb.batch.sectors', string='Sector Interested', required=True)
    course_id = fields.Many2one('amb.courses', string='Job Role Course Trade', required=True)
    edu_level = fields.Many2one('amb.student.education.level', string='Education Level', required=True)
    institute_id = fields.Many2one('res.company', string='Institute')
    enquiry_id = fields.Char(string="Enquiry ID", readonly=True, required=True, 
                            default=lambda self: _('New'))
    salutation_id = fields.Many2one('amb.student.salutation' ,string='Salutation')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'IN')], limit=1)
        return country

    def student_inprogress(self):
        self.states='in_progress'
        student_obj = self.env['res.partner']
        student_id = student_obj.create({
                   'name': self.name,
                   'salutation_id': self.salutation_id.id,
                   'first_name': self.first_name,
                   'middle_name': self.middle_name,
                   'last_name': self.last_name,
                   'enquiry_id':self.enquiry_id,
                   'course_id':self.course_id.id,
                   'mobile':self.mobile,
                   'email':self.email,
                   'phone':self.mobile_parent,
                   'edu_level':self.edu_level.id,
                   'dob':self.dob,
                   'gender':self.gender,
                   'father_name':self.parent_name,
                   'institute_id' :self.institute_id.id,
                   'aadhar_no': self.aadhar_no,
                   'street':self.street,
                   'street2':self.street2,
                   'city':self.city,
                   'state_id':self.state_id.id,
                   'zip':self.zip,
                   'country_id':self.country_id.id,
                   'is_student' : False,
                   'company_type': 'person',
                   'parent_id':self.institute_id.partner_id.id,
               })
        self.student_id=student_id

    def student_confirm(self):
        self.states='enrolled'
        self.student_id.is_student=True

    def student_reset(self):
        self.states='new'

    def student_cancel(self):
        self.states='cancel'


    def view_student(self):
        """
        This function returns Kanban/Tree/Form view action of student enrolled from enquiry.
        """
        form_view = self.env.ref('ambuja_core.amb_res_partner_form').id
        return {
        'name': (self.name),
        'type': 'ir.actions.act_window',
        'res_model': 'res.partner',
        'views': [(form_view or False, 'form')],
        'res_id': self.student_id.id
        }
    
    
    regex_string="^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$"
    regex_number="[7-9][0-9]{9}"
    regex_mail="^(\D)+(\w)*((\.(\w)+)?)+@(\D)+(\w)*((\.(\D)+(\w)*)+)?(\.)[a-z]{2,}$"
    
    @api.constrains('dob')
    def _check_birthdate(self):
        if self.dob > fields.Date.today():
            raise UserError("Birth Date should be less than current date!")
        elif self.dob == fields.Date.today():
            raise UserError("Birth Date should be  less than current date!")
    @api.onchange('dob')
    def _onchange_dob(self):
        if self.dob and self.dob > fields.Date.today():
            return {
                    'warning': {
                        'title': _('Validation Error'),
                        'message': _("Birth Date should be less than current date!")
                                }
                    }
        elif self.dob and self.dob == fields.Date.today():
            return {
                    'warning': {
                        'title': _('Validation Error'),
                        'message': _("Birth Date should be  less than current date!")
                                }
                    }
    
    @api.constrains('first_name','aadhar_no','middle_name','last_name','parent_name')
    def _validate_name(self):
        if not str(self.first_name).isalpha():
            raise UserError("Please don't use Space or any Special Character in First Name")
        if self.aadhar_no and not str(self.aadhar_no).isnumeric():
            raise UserError('Please use only numbers in Aadhar Number')
        if self.middle_name and (re.match(self.regex_string, self.middle_name) == None):
            raise UserError("Please don't use any Special Character in Middle Name")
        if self.last_name and (re.match(self.regex_string, self.last_name) == None):
            raise UserError("Please don't use any Special Character in Last Name")
        if self.parent_name and (re.match(self.regex_string, self.parent_name) == None):
            raise UserError("Please don't use any Special Character in Parent Name")

    @api.onchange('first_name','middle_name','last_name','parent_name','aadhar_no','salutation_id')
    def _onchange_name(self):
        
        if not str(self.first_name).isalpha():
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please don't use Space or any Special Character in First Name")
                                }
                    }
        if self.middle_name and (re.match(self.regex_string, self.middle_name) == None):
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please don't use any Special Character in Middle Name")
                                }
                    }
        if self.last_name and (re.match(self.regex_string, self.last_name) == None):
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please don't use any Special Character in Last Name")
                                }
                    }
        if self.parent_name and (re.match(self.regex_string, self.parent_name) == None):
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please don't use any Special Character in Parent Name")
                                }
                    }
        if self.aadhar_no and not str(self.aadhar_no).isnumeric():
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please use only numbers in Aadhar Number")
                                }
                    }

        if (not self.middle_name) and (not self.last_name):
            self.name = str(self.salutation_id.name) + " " + str(self.first_name)

        elif not self.middle_name:
            self.name = str(self.salutation_id.name) + " " + str(self.first_name) + " " + str(
                        self.last_name)

        elif not self.last_name:
            self.name = str(self.salutation_id.name) + " " + str(self.first_name) + " " + str(
                        self.middle_name)
        
        else:
            self.name = str(self.salutation_id.name) + " " + str(self.first_name) + " " + str(
                self.middle_name) + " " + str(self.last_name)
        
    # @api.constrains('mobile','mobile_parent')
    # def _validate_mobile(self):
    #     if not str(self.mobile).isnumeric():
    #         raise UserError("Please use only numbers in Contact No.")
    #     elif (re.match(self.regex_number, str(self.mobile)) == None):
    #         raise UserError("Enter valid 10 digits Contact No.")
    #     if self.mobile_parent:
    #         if (re.match(self.regex_number, str(self.mobile_parent)) == None):
    #             raise UserError("Enter valid 10 digits Parent Contact No.")
    
    @api.onchange('mobile')
    def _onchange_mobile(self):
        if self.mobile and not str(self.mobile).isnumeric():
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please use only numbers in Contact No.")
                                }
                    }
        elif self.mobile and (re.match(self.regex_number, str(self.mobile)) == None):
            return {
                    'warning': {
                        'title': _('Validation Error'),
                        'message': _("Enter valid 10 digits Contact No.")
                                }
                    }
        if self.mobile and len(self.mobile) == 10:
            existing_number_mobile = self.env['amb.enquiry'].search([('mobile','=', self.mobile),
                                    ('mobile_parent','=', self.mobile)])
            if existing_number_mobile:
                return {
                        'warning': {
                            'title': _('Data Duplication Error'),
                            'message': _("Contact No. Already exist, Please check.")
                                    }
                        }
        
    @api.onchange('mobile_parent')
    def _onchange_mobile_parent(self):
        if self.mobile_parent and not str(self.mobile_parent).isnumeric():
            return {
                    'warning': {
                        'title': _('Type Error'),
                        'message': _("Please use only numbers in Parent Contact No.")
                                }
                    }
        elif self.mobile_parent and (re.match(self.regex_number, str(self.mobile_parent)) == None):
            return {
                    'warning': {
                        'title': _('Validation Error'),
                        'message': _("Enter valid 10 digits Parent Contact No.")
                                }
                    }
        if self.mobile_parent and len(self.mobile_parent)== 10:
            existing_number_mobile_parent = self.env['amb.enquiry'].search([('mobile','=', self.mobile_parent),
                                    ('mobile_parent','=', self.mobile_parent)])
            if existing_number_mobile_parent:
                return {
                    'warning': {
                        'title': _('Data Duplication Error'),
                        'message': _("Parent Contact No. Already exist, Please check.")
                    }
                }
            if self.mobile == self.mobile_parent:
                return {
                    'warning': {
                        'title': _('Duplication Error'),
                        'message': _("Mobile No. and Parent Contact No. are same")
                    }
                }
    @api.constrains('email')
    def _validate_email(self):
        if self.email and (re.match(self.regex_mail, self.email) == None):
            raise UserError("Enter valid Email")
    
    @api.onchange('email')
    def _onchange_email(self):
        if self.email and (re.match(self.regex_mail, self.email) == None):
            return {
                    'warning': {
                        'title': _('Validation Error'),
                        'message': _("Enter valid Email")
                                }
                    }
    @api.model
    def create(self, vals):
        if vals.get('enquiry_id', _('New')) == _('New'):
            vals['enquiry_id'] = self.env['ir.sequence'].next_by_code('enquiry.id') or _('New')
        result = super(Enquiry, self).create(vals)
        return result

    def create_student_action(self):
        enquiry_ids=self.env['amb.enquiry'].browse(self.ids)
        student_obj = self.env['res.partner']
        for enquiry_id in enquiry_ids:
            if enquiry_id.states == 'in_progress' or enquiry_id.states == 'new':
                student_id = student_obj.create({
                   'name': enquiry_id.name,
                   'salutation_id': enquiry_id.salutation_id.id,
                   'first_name': enquiry_id.first_name,
                   'middle_name': enquiry_id.middle_name,
                   'last_name': enquiry_id.last_name,
                   'enquiry_id':enquiry_id.enquiry_id,
                   'course_id':enquiry_id.course_id.id,
                   'mobile':enquiry_id.mobile,
                   'email':enquiry_id.email,
                   'phone':enquiry_id.mobile_parent,
                   'edu_level':enquiry_id.edu_level.id,
                   'dob':enquiry_id.dob,
                   'gender':enquiry_id.gender,
                   'father_name':enquiry_id.parent_name,
                   'institute_id' :enquiry_id.institute_id.id,
                   'aadhar_no': enquiry_id.aadhar_no,
                   'street':enquiry_id.street,
                   'street2':enquiry_id.street2,
                   'city':enquiry_id.city,
                   'state_id':enquiry_id.state_id.id,
                   'zip':enquiry_id.zip,
                   'country_id':enquiry_id.country_id.id,
                   'is_student' : True,
                   'company_type': 'person',
                   'parent_id':enquiry_id.institute_id.partner_id.id,
                })
                enquiry_id.student_id=student_id
                enquiry_id.states='enrolled'