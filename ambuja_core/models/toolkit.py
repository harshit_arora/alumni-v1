# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Toolkit(models.Model):
    _name = 'amb.toolkit'
    _inherit = "mail.thread"
    _description = 'Student Toolkit'
    _rec_name = 'name'

    name = fields.Char(string='Name', required=True)
    course_id = fields.Many2one('amb.courses', string='Course', required=True)
    tools_line_ids = fields.One2many('amb.tools.lines', 'tool_id', string='Tools')
    active = fields.Boolean('Active', default=True) 


class ProductLines(models.Model):
    _name = 'amb.tools.lines'
    _description = 'Fees Lines'
    _rec_name = 'product_id'

    tool_id = fields.Many2one('amb.toolkit', string='Tool ID')
    product_id = fields.Many2one('product.product', string='Tool Name', required=True)
    quantity = fields.Integer(string='Quantity', required=True)