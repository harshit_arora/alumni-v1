# -*- coding: utf-8 -*-

from odoo import models, fields


class Religion(models.Model):
    _name = "amb.student.religion"
    _inherit = "mail.thread"
    _description = "Religion Information"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 