# -*- coding: utf-8 -*-

from odoo import models, fields


class Courses(models.Model):
    _name = "amb.courses"
    _inherit = "mail.thread"
    _description = "Courses Information"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    s_covered = fields.Many2one('amb.batch.sectors', string='Sector Covered', required=True)
    course_code = fields.Many2one('amb.course.code', string='Course Code', required=True)
    toolkit_req = fields.Boolean(string='Toolkit Required')
    active = fields.Boolean('Active', default=True)