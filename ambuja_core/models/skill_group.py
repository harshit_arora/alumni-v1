# -*- coding: utf-8 -*-

from odoo import models, fields


class SkillGroup(models.Model):
    _name = "amb.skill.group"
    _inherit = "mail.thread"
    _description = "Student Skill Groups"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 