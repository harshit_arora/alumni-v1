# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Trainer(models.Model):
    _description = "Trainer Information"
    _inherit = "hr.employee"
    _rec_name = "name"

    # attributes
    is_employee= fields.Boolean(string='Is Employee', default=False)
    is_trainer = fields.Boolean(string='Is Trainer', default=False)
    t_id = fields.Char(string='Trainer ID')
    edu = fields.Char(string='Education')
    y_of_pre_ex = fields.Integer(
        string='Number of Years of Previous Experience')
    m_of_pre_ex = fields.Integer(
        string='Number of Months of Previous Experience')
    disc = fields.Selection([
        ('y', 'YES'),
        ('n', 'NO')
    ], string='Discontinued', default='n')
    remarks = fields.Text(string='Remarks')
    joining_date = fields.Date(string='Joining Date')
    period_appointment = fields.Date(string='Period of Appointment')
    nature_contract = fields.Char(string='Nature of Contract')
    prev_experience_months = fields.Char(string='No. of months of previous experience')
    prev_experience_years = fields.Char(string='No. of years of previous experience')
    discontinued= fields.Boolean(string='Discontinued', default=False)

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')

    institute_id = fields.Many2one('res.company', string='Institute')
    course_id = fields.Many2one('amb.courses', string='Job Role Course Trade')

    @api.onchange('course_id')
    def _onchange_course_id(self):
        if self.course_id:
            self.job_title = self.course_id.name
            
    @api.onchange('institute_id')
    def _onchange_institute_id(self):
        if self.institute_id:
            self.address_id = self.institute_id.partner_id

    def employee_reset(self):
        self.states='draft'
        if self.is_trainer == True:
            self.is_trainer=False

    def employee_done(self):
        self.states='done'
        if (self.job_id.name).lower() == 'trainer':
            self.is_trainer=True
        self.is_employee=True