# -*- coding: utf-8 -*-

from odoo import models, fields


class MobilizationSource(models.Model):
    _name = "amb.mobilization.source"
    _inherit = "mail.thread"
    _description = "Mobilization Source"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 