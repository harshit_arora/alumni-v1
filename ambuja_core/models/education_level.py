# -*- coding: utf-8 -*-

from odoo import models, fields


class EducationLevel(models.Model):
    _name = "amb.student.education.level"
    _inherit = "mail.thread"
    _description = "Education Levels"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 