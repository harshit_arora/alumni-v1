# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
class Batch(models.Model):
    _name = "amb.batch"
    _inherit = "mail.thread"
    _description = "Batch Information"
    _rec_name = "name"

    name = fields.Char(string='Batch Name', readonly=True, required=True, 
                            default=lambda self: _('New'))
    s_covered = fields.Many2one('amb.batch.sectors', string='Sector Covered', required=True)
    course_id = fields.Many2one('amb.courses', string='Job Role Course Trade', required=True)
    b_s_date = fields.Date(string='Start Date',
                           default=fields.Date.today(), required=True)
    b_e_date = fields.Date(string='End Date', required=True)
    duration = fields.Char(string='Duration', required=True)
    batch_fee = fields.Float(string='Fee', digits=0, required=True)
    t_id = fields.Many2one('hr.employee', string='Trainer', required=True)
    remarks = fields.Text(string='Remarks')
    institute_id = fields.Many2one('res.company', string='Institute')
    approved_date = fields.Date(string='Approved Date')
    states = fields.Selection([
        ('draft', 'Draft'),
        ('waiting','Waiting for Approval'),
        ('approved','Approved'),
        ('started','Started'),
        ('closed','Closed'),
        ('rejected','Rejected')
    ], string='States',default='draft')

    def batch_waiting(self):
        self.states='waiting'
    
    def batch_approved(self):
        self.states='approved'
        self.approved_date = fields.Date.today()
    
    def batch_started(self):
        self.states='started'
    
    def batch_closed(self):
        self.states='closed'
    
    def batch_reject(self):
        self.states='rejected'
    
    def batch_reset(self):
        self.states='draft'

    @api.model
    def create(self, vals):
        if vals.get('name', _('New')) == _('New'):
            district=self.env['res.company'].search([('id','=', str(vals['institute_id']))]).district_code
            course=self.env['amb.courses'].search([('id','=', str(vals['course_id']))]).course_code.name
            date=str(vals['b_s_date'])
            year=str(int(date[:4]))
            vals['name'] = district + '/' + course + '/' + year + '-' + str(int(year[-2:]) + 1) + '/' +self.env['ir.sequence'].next_by_code('batch.name') or _('New')
        result = super(Batch, self).create(vals)
        return result
    
    @api.onchange('b_e_date','b_s_date')
    def _onchange_b_e_date(self):
        if self.b_e_date:
            if self.b_e_date < self.b_s_date:
                raise UserError("End Date cannot be less than Start Date")
            elif self.b_e_date == self.b_s_date:
                raise UserError("End Date cannot be equal to Start Date")
            else:
                def last_day_of_month(date):
                    if date.month == 12:
                        return date.replace(day=31)
                    return date.replace(month=date.month+1, day=1) - timedelta(days=1)
                b_s_date = datetime.strptime(str(self.b_s_date), '%Y-%m-%d')
                b_e_date = datetime.strptime(str(self.b_e_date), '%Y-%m-%d')
                last_month_date=b_e_date
                days=0
                m1=b_s_date.year*12+b_s_date.month
                m2=b_e_date.year*12+b_e_date.month
                months=m2-m1
                if b_s_date.day>b_e_date.day:
                    months-=1
                    if b_e_date.month != 1:
                        last_day = last_day_of_month(last_month_date.replace(month=last_month_date.month-1,day=1)).day
                    else:
                        last_day = last_day_of_month(last_month_date.replace(month=12,day=1)).day
                    if b_e_date.day == last_day_of_month(b_e_date).day:
                        months+=1
                    elif  last_day < b_e_date.day :
                        min = b_e_date.day - last_day
                        last_month_date=last_month_date.replace(month=last_month_date.month-1,day=last_month_date.day-min)
                    elif last_day < b_s_date.day:
                        min = b_s_date.day - last_day
                        last_month_date=last_month_date.replace(month=last_month_date.month-1,day=b_s_date.day-min)
                    elif b_e_date.month == 1:
                        last_month_date=last_month_date.replace(month=12,day=b_s_date.day,year=b_s_date.year)
                    else:
                        last_month_date=last_month_date.replace(month=last_month_date.month-1,day=b_s_date.day)
                    days=(b_e_date - last_month_date).days                  
                elif b_s_date.day==b_e_date.day:
                    seconds1=b_s_date.hour*3600+b_s_date.minute+b_s_date.second
                    seconds2=b_e_date.hour*3600+b_e_date.minute+b_e_date.second
                    if seconds1>seconds2:
                        months-=1
                else:
                    if b_e_date.day == last_day_of_month(b_e_date).day and b_s_date.day == 1:
                        months+=1
                    else:
                        days=b_e_date.day-b_s_date.day 
                if days != 0:
                    if months !=0:
                        if months > 1:
                            self.duration = str(months) +" Months " + str(days) +" Days" 
                            # self.duration = str(months) +"." + str(days/31)[2:4]+" Months"
                        else:
                            self.duration = str(months) +" Month " + str(days) +" Days"
                    else:
                        self.duration = str(days) +" Days" 
                else:
                    if months > 1:
                        self.duration = str(months) +" Months"
                    else:
                        self.duration = str(months) +" Month"