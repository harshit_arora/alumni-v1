# -*- coding: utf-8 -*-

from odoo import models, fields


class FiscalYear(models.Model):
    _name = "amb.fiscal.year"
    _inherit = "mail.thread"
    _description = "Fiscal Year Information"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 


class AgeGroup(models.Model):
    _name = "age.group"
    _inherit = "mail.thread"
    _description = "Age Group"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    start_age = fields.Integer(string="Start Age", required=True)
    end_age = fields.Integer(string="End Age", required=True) 