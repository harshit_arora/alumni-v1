# -*- coding: utf-8 -*-

from odoo import models, fields


class AnnualIncome(models.Model):
    _name = "amb.student.annual.income"
    _inherit = "mail.thread"
    _description = "Annual Household Income Information"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 