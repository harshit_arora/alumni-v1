# -*- coding: utf-8 -*-

from odoo import models, fields, api, _


class Assessment(models.Model):
    _name = 'amb.assessment'
    _inherit = "mail.thread"
    _description = 'Student Marksheets'
    _rec_name = 'student_name_id'

    result = fields.Selection([
        ('y', 'YES'),
        ('n', 'NO')
    ], string='Result Pass', default='y', required=True)
    actual_marks = fields.Float(string='Actual Marks', required=True)
    outof_total_marks = fields.Integer(string='Out of Total Marks', required=True)
    percentage = fields.Float(string='Percentage of Marks', required=True)
    attendance_percent = fields.Integer(string='Attendance Percentage', required=True)
    passing_date = fields.Date(string='Passing Out Date', required=True)
    certified = fields.Selection([
        ('y', 'YES'),
        ('n', 'NO')
    ], string='Certified', default='y', required=True)
    grade = fields.Selection([
        ('a1', 'A1'),
        ('a', 'A'),
        ('b1', 'B1'),
        ('b', 'B'),
        ('c', 'C'),
        ('d', 'D'),
        ('o', 'O')
    ], string='Grade', default='a1', required=True)
    certification_date = fields.Date(string='Certification Date')
    certificate_name = fields.Char(string='Certificate Name or Award')
    certificate_no = fields.Char(string='Certificate No.')
    assessment_date = fields.Date(string='Assessment Date', required=True)
    agency = fields.Char(string='Agency')
    assessor = fields.Char(string='Assessor')
    certifying_agency = fields.Char(string='Certifying Agency')
    remarks = fields.Text(string='Remarks')
    marksheet_no = fields.Char(string="Marksheet No.", readonly=True, required=True, 
                            default=lambda self: _('New'))

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')

    # Related Fields
    batch_name_id = fields.Many2one('amb.batch', string='Batch', required=True)
    student_name_id = fields.Many2one('res.partner', string='Student', required=True)
    course_id = fields.Many2one(related='student_name_id.course_id', string='Course')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
    b_e_date = fields.Date(string='Batch End Date', related='batch_name_id.b_e_date')
    tp_enrollment_no = fields.Char(string="Enrollment No.",related='student_name_id.tp_enrollment_no')
    institute_id = fields.Many2one('res.company', string='Institute')
    image_1920 = fields.Binary(related='student_name_id.image_1920',
                               string='Student Image')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    @api.onchange('actual_marks','outof_total_marks')
    def on_change_marks(self):
        if(self.outof_total_marks <= 0):
            self.percentage = 0
        else:
            self.percentage=(self.actual_marks/self.outof_total_marks)*100

    @api.onchange('percentage')
    def on_change_percentage(self):
        if (self.percentage >= 40):
            self.result = 'y'
        else:
            self.result = 'n'
    
    @api.model
    def create(self, vals):
        if vals.get('marksheet_no', _('New')) == _('New'):
            vals['marksheet_no'] = self.env['ir.sequence'].next_by_code('marksheet.no') or _('New')
        result = super(Assessment, self).create(vals)
        return result

    def student_reset(self):
        self.states='draft'
    
    def student_done(self):
        self.states='done'