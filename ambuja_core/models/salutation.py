# -*- coding: utf-8 -*-

from odoo import models, fields


class EducationLevel(models.Model):
    _name = "amb.student.salutation"
    _inherit = "mail.thread"
    _description = "Salutations"
    _rec_name = "name"

    name = fields.Char(string='Name', required=True)
    active = fields.Boolean('Active', default=True) 