# -*- coding: utf-8 -*-

from odoo import models, fields, api, _
from odoo.exceptions import UserError
from datetime import datetime,timedelta
import re

class ResStudent(models.Model):
	_inherit = 'res.partner'
	_description = 'Student Information'
	_rec_name = "name"

	# Calculated age
	@api.depends('dob')
	def age_calc(self):
		age_groups = self.env['age.group'].search([])
		if self.dob is not False:
			age = (datetime.today().date() - datetime.strptime(str(self.dob), '%Y-%m-%d').date()) // timedelta(days=365)
			self.calc_age = age
			for group in age_groups:
				if age >= group.start_age and age <= group.end_age:
					self.age_group_id = group.id

	states = fields.Selection([
		('new', 'New'),
		('in_progress','In Progress'),
		('on_hold','On Hold'),
		('approved','Approved'),
		('registered','Registered'),
		('alumni','Alumni'),
		('drop_out','Drop Out'),
		('reject','Reject')
	], string='States',default='new')
	enquiry_id = fields.Char(string='Enquiry_id')
	first_name = fields.Char('First Name', size=128, translate=True)
	middle_name = fields.Char('Middle Name', size=128, translate=True)
	last_name = fields.Char('Last Name', size=128, translate=True)
	place_birth = fields.Char(string='Place of Birth')
	father_name = fields.Char(string="Father's Name")
	mother_name = fields.Char(string="Mother's Name")
	maritial_status = fields.Selection([
		('married', 'Married'),
		('single', 'Single'),
		('divorced','Divorced'),
		('seperated','Seperated'),
		('widowed','Widowed')
	], string='Marital Status', default='single')
	dob = fields.Date(string='Dob')
	calc_age = fields.Integer(string="Age", compute="age_calc", store=True)
	age_group_id = fields.Many2one('age.group', string='Age Group')
	gender = fields.Selection([
		('m', 'Male'),
		('f', 'Female'),
		('o', 'Other')
	], string='Gender', default='m')
	pre_training_status = fields.Selection([
		('experienced', 'Experienced'),
		('fresher', 'Fresher')
	], string='Pre Training Status')
	edu_level = fields.Many2one('amb.student.education.level', string='Education Level')
	fees = fields.Integer(string='Course Fee')
	below_pov_line = fields.Selection([
		('y', 'YES'),
		('n', 'NO')
	], string='Below Poverty Line')
	drop_date = fields.Date(string='Dropped Date')
	aadhar_no = fields.Char(string='Aadhar Number', size=12)
	aadhar_enrollment_no = fields.Char(string='Aadhar Enrollment Number', size=12)
	caste = fields.Selection([
		('sc', 'SC'),
		('st', 'ST'),
		('obc', 'OBC'),
		('gen', 'Gen'),
		('ph','PH'),
		('nt','NT')
	], string='Caste Category', default='gen')
	disability = fields.Boolean(string='Disability')
	religion = fields.Many2one('amb.student.religion', string='Religion')
	y_of_pre_ex = fields.Integer(
		string='No of years of previous experience', size=2)
	m_of_pre_ex = fields.Integer(
		string='No of months of previous experience', size=2)
	tech_edu = fields.Selection([
		('y', 'Yes'),
		('n', 'No')
	], string='Technical Education')
	ration_no = fields.Char(string='Ration Card Number', size=14)
	t_mobile = fields.Selection([
		('s', 'Smart Phone'),
		('o', 'Other'),
		('n', 'None')
	], string='Type of Mobile', default='n')
	pass_no = fields.Char(string='Passport Number')
	val_date = fields.Date(string='Passport Validity Date')
	fam_ocp = fields.Selection([
		('a/o', 'Agriculture/Own Farming'),
		('self', 'Self Employed'),
		('aw', 'Agriculture Wages'),
		('gj', 'Government Job'),
		('l', 'Labour'),
		('p', 'Private Job'),
		('o', 'Other')
	], string='Family Occupation', default='a/o')
	no_of_placement = fields.Integer(string='No. of Placement')
	sdms_enrol_no = fields.Char(string='SDMS Enrollment Number')
	bank_name = fields.Char(string='Bank Name')
	bank_branch_address = fields.Char(string='Bank Branch Address')
	account_number = fields.Char(string='Bank Account No.')
	ifsc_code = fields.Char(string='IFSC Code')
	remarks = fields.Text(string='Remarks')
	toolkit_assign = fields.Boolean(string='Assign Toolkit')
	fee_paid_by = fields.Selection([
		('self', 'Self-Paid'),
		('central', 'Central Government'),
		('csr','CSR'),
		('individual','Individual Sponsor'),
		('industry','Industry Sponsored'),
		('state','State Government'),
		('other','Other corporate & NGO'),
		('gov','Government Sponsored'),
		('power_grid','Power Grid'),
		('trust','Trust')
	], string='Fee Paid By')
	source_funding = fields.Char(string='Source of Funding')
	sponsored_by = fields.Selection([
		('self', 'Self-Paid'),
		('industry','Industry Sponsored'),
		('state','State Government'),
		('other','Other corporate & NGO'),
		('gov','Government Sponsored'),
		('power_grid','Power Grid'),
		('trust','Trust')
	], string='Sponsored By')

	is_student = fields.Boolean(string='Is Student', default=False)
	invoice_created = fields.Boolean(string='Invoice Created', default=False)

	tp_enrollment_no = fields.Char(string="Enrollment No.", 
							default=lambda self: _('New'))
	annual_h_income = fields.Many2one('amb.student.annual.income', string='	Annual Household Income')
	course_id = fields.Many2one('amb.courses', string='Course', help="Job Role Course Trade")
	batch_name_id = fields.Many2one('amb.batch', string='Batch')
	s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
	b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
	b_e_date = fields.Date(string=' Batch End Date', related='batch_name_id.b_e_date')
	institute_id = fields.Many2one('res.company', string='Institute')
	skill_group_ids = fields.Many2many('amb.skill.group', string='Skill Groups')
	salutation_id = fields.Many2one('amb.student.salutation' ,string='Salutation')
	fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')

	def _call_update_age(self):
		""" Called by cron job"""
		age_groups = self.env['age.group'].search([])
		students = self.env['res.partner'].search([('is_student', '=', True)])
		for student in students:
			if student.dob is not False:
				age = (datetime.today().date() - datetime.strptime(str(student.dob), '%Y-%m-%d').date()) // timedelta(days=365)
				for group in age_groups:
					if age >= group.start_age and age <= group.end_age:
						student.write({'calc_age': age, 'age_group_id': group.id})
					else:
						student.write({'calc_age': age})


	@api.onchange('institute_id')
	def on_change_institute_id(self):
		country = self.env['res.country'].search([('code', '=', 'IN')], limit=1)
		self.country_id=country
		self.parent_id=self.institute_id.partner_id.id

	def student_inprogress(self):
		self.states='in_progress'
		self.is_student=True
		if self.tp_enrollment_no == _('New'):
			self.tp_enrollment_no = str(self.batch_name_id.name) + "/" +self.env['ir.sequence'].next_by_code('tpenrollment.no') or _('New')
	
	def student_onhold(self):
		self.states='on_hold'
	
	def student_reject(self):
		self.states='reject'
	
	def student_reset(self):
		self.states='new'
	
	def student_approved(self):
		self.states='approved'
	
	def student_dropout(self):
		self.states='drop_out'
	
	def student_registered(self):
		self.states='registered'
	
	def student_alumni(self):
		self.states='alumni'
		today_date=fields.Date.today()
		def last_day_of_month(date):
					if date.month == 12:
						return date.replace(day=31)
					return date.replace(month=date.month+1, day=1) - timedelta(days=1)
		for num in range(5):
			followup_id=self.env['amb.followup'].create({
				'institute_id':self.institute_id.id,
				'student_id': self.id,
				'course_id' : self.course_id.id,
				'batch_name_id': self.batch_name_id.id
				})
			if num == 0:
				if today_date.month <=10 :
					followup_date = today_date.replace(month=today_date.month+2,day=1)
				elif today_date.month == 11 :
					followup_date = today_date.replace(month=1,
									year=today_date.year+1,day=1)
				else:
					followup_date = today_date.replace(month=2,
									year=today_date.year+1,day=1)
				if (today_date.day == last_day_of_month(today_date).day and 
				today_date.day > last_day_of_month(followup_date).day):
					followup_date = followup_date.replace(day=last_day_of_month(followup_date).day)
				else:
					followup_date = followup_date.replace(day=today_date.day)
				followup_id.followup_date = followup_date
				followup_id.name="2 Month Followup"
			
			elif num == 1:
				if today_date.month <=6 :
					followup_date = today_date.replace(month=today_date.month+6,day=1)
				else:
					month=today_date.month-6
					followup_date = today_date.replace(month=month,
									year=today_date.year+1,day=1)
				if (today_date.day == last_day_of_month(today_date).day and 
				today_date.day > last_day_of_month(followup_date).day):
					followup_date = followup_date.replace(day=last_day_of_month(followup_date).day)
				else:
					followup_date = followup_date.replace(day=today_date.day)
				followup_id.followup_date = followup_date
				followup_id.name="6 Month Followup"

			elif num == 2:
				followup_date = today_date.replace(month=today_date.month,
											day=1,year=today_date.year+1)
				if today_date.month == 2:
					if (today_date.day == last_day_of_month(today_date).day and 
						today_date.day > last_day_of_month(followup_date).day):
						followup_date = followup_date.replace(day=last_day_of_month(followup_date).day)
					else:
						followup_date = followup_date.replace(day=today_date.day)
				else:
					followup_date = followup_date.replace(day=today_date.day)
				followup_id.followup_date=followup_date
				followup_id.name="12 Month Followup"
			
			elif num == 3:
				if today_date.month <=6 :
						followup_date = today_date.replace(month=today_date.month+6,
										year=today_date.year+1,day=1)
				else:
					month=today_date.month-6
					followup_date = today_date.replace(month=month,
									year=today_date.year+2,day=1)
				if (today_date.day == last_day_of_month(today_date).day and 
				today_date.day > last_day_of_month(followup_date).day):
					followup_date = followup_date.replace(day=last_day_of_month(followup_date).day)
				else:
					followup_date = followup_date.replace(day=today_date.day)
				followup_id.followup_date = followup_date
				followup_id.name="18 Month Followup"

			elif num == 4:
				followup_date = today_date.replace(month=today_date.month,
											day=1,year=today_date.year+2)
				if today_date.month == 2:
					if (today_date.day == last_day_of_month(today_date).day and 
						today_date.day > last_day_of_month(followup_date).day):
						followup_date = followup_date.replace(day=last_day_of_month(followup_date).day)
					else:
						followup_date = followup_date.replace(day=today_date.day)
				else:
					followup_date = followup_date.replace(day=today_date.day)
				followup_id.followup_date = followup_date
				followup_id.name="24 Month Followup"

	def view_toolkit(self):
		"""
		This function returns Tree view action of student's Toolkit.
		"""
		# form_view = self.env.ref('ambuja_core.student_toolkit_view_form').id
		return {
		'name': (self.name),
		'type': 'ir.actions.act_window',
		'res_model': 'amb.toolkit',
		'view_mode': 'tree,form',
		'domain': [('course_id', '=', self.course_id.id)],
		# 'views': [(form_view or False, 'form')],
		# 'res_id': self.course_id.id,
		}
	
	def assign_toolkit(self):
		self.toolkit_assign=True
	
	def view_followup(self):
		"""
		This function returns Tree view action of student's Followup.
		"""
		return {
		'name': (self.name),
		'type': 'ir.actions.act_window',
		'res_model': 'amb.followup',
		'view_mode': 'tree,form',
		'domain': [('student_id', '=', self.id)],
		}

	def view_placement_support(self):
		"""
		This function returns Tree view action of student's Placement
		Support Data.
		"""
		return {
		'name': (self.name),
		'type': 'ir.actions.act_window',
		'res_model': 'amb.placement.support',
		'view_mode': 'tree,form',
		'domain': [('student_id', '=', self.id)],
		}
	
	def view_fees(self):
		"""
		This function returns Tree view action of student's Fees.
		"""
		tree_view = self.env.ref('ambuja_core.amb_view_invoice_tree').id
		form_view = self.env.ref('ambuja_core.amb_view_move_form').id
		return {
		'name': (self.name),
		'type': 'ir.actions.act_window',
		'res_model': 'account.move',
		'views': [(tree_view, 'tree'),(form_view, 'form')],
		'domain': [('partner_id', '=', self.id),('type', '=', 'out_invoice')],
		}
		
	regex_string="^[a-zA-Z-,]+(\s{0,1}[a-zA-Z-, ])*$"
	regex_number="[7-9][0-9]{9}"
	regex_mail="^(\D)+(\w)*((\.(\w)+)?)+@(\D)+(\w)*((\.(\D)+(\w)*)+)?(\.)[a-z]{2,}$"

	@api.constrains('first_name','aadhar_no','middle_name','last_name',
					'father_name','mother_name','account_number','pass_no')
	def _validate_name(self):
		if not str(self.first_name).isalpha():
			raise UserError("Please don't use Space or any Special Character in First Name")
		if self.middle_name and (re.match(self.regex_string, self.middle_name) == None):
			raise UserError("Please don't use any Special Character in Middle Name")
		if self.last_name and (re.match(self.regex_string, self.last_name) == None):
			raise UserError("Please don't use any Special Character in Last Name")
		if self.father_name and (re.match(self.regex_string, self.father_name) == None):
			raise UserError("Please don't use any Special Character in Father Name")
		if self.mother_name and (re.match(self.regex_string, self.mother_name) == None):
			raise UserError("Please don't use any Special Character in Mother Name")
		if self.aadhar_no and not str(self.aadhar_no).isnumeric():
			raise UserError('Please use only numbers in Aadhar Number')
		if self.account_number and not str(self.account_number).isnumeric():
			raise UserError('Please use only numbers in Bank A/C Number')
		if self.pass_no and not str(self.pass_no).isnumeric():
			raise UserError('Please use only numbers in Passport Number')
	
	@api.onchange('first_name','middle_name','last_name','salutation_id',
				  'aadhar_no','father_name','mother_name','account_number','pass_no')
	def _onchange_name(self):
		
		if not str(self.first_name).isalpha():
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please don't use Space or any Special Character in First Name")
								}
					}
		if self.middle_name and (re.match(self.regex_string, self.middle_name) == None):
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please don't use any Special Character in Middle Name")
								}
					}
		if self.last_name and (re.match(self.regex_string, self.last_name) == None):
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please don't use any Special Character in Last Name")
								}
					}
		if self.father_name and (re.match(self.regex_string, self.father_name) == None):
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please don't use any Special Character in Father Name")
								}
					}
		if self.mother_name and (re.match(self.regex_string, self.mother_name) == None):
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please don't use any Special Character in Mother Name")
								}
					}
		if self.aadhar_no and not str(self.aadhar_no).isnumeric():
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please use only numbers in Aadhar Number")
								}
					}
		if self.account_number and not str(self.account_number).isnumeric():
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please use only numbers in Bank A/C Number")
								}
					}
		if self.pass_no and not str(self.pass_no).isnumeric():
			return {
					'warning': {
						'title': _('Type Error'),
						'message': _("Please use only numbers in Passport Number")
								}
					}

		if (not self.middle_name) and (not self.last_name):
			self.name = str(self.salutation_id.name) + " " + str(self.first_name)

		elif not self.middle_name:
			self.name = str(self.salutation_id.name) + " " + str(self.first_name) + " " + str(
						self.last_name)

		elif not self.last_name:
			self.name = str(self.salutation_id.name) + " " + str(self.first_name) + " " + str(
						self.middle_name)
		
		else:
			self.name = str(self.salutation_id.name) + " " + str(self.first_name) + " " + str(
				self.middle_name) + " " + str(self.last_name)
		
	@api.constrains('dob')
	def _check_birthdate(self):
		if self.dob > fields.Date.today():
			raise UserError("Birth Date should be less than current date!")
		elif self.dob == fields.Date.today():
			raise UserError("Birth Date should be  less than current date!")
		
	@api.onchange('dob')
	def _onchange_dob(self):
		if self.dob and self.dob > fields.Date.today():
			return {
					'warning': {
						'title': _('Validation Error'),
						'message': _("Birth Date should be less than current date!")
								}
					}
		elif self.dob and self.dob == fields.Date.today():
			return {
					'warning': {
						'title': _('Validation Error'),
						'message': _("Birth Date should be  less than current date!")
								}
					}
	
	# @api.constrains('mobile','phone')
	# def _validate_mobile(self):
	#     if self.mobile and not str(self.mobile).isnumeric():
	#         raise UserError("Please use only numbers in Mobile No.")
	#     elif ((self.mobile and (re.match(self.regex_number, str(self.mobile)) == None)) or 
	#         (self.mobile and len(self.mobile) != 10)):
	#         raise UserError("Enter valid 10 digits Mobile No.")
	#     if self.phone and not str(self.phone).isnumeric():
	#         raise UserError("Please use only numbers in Parent Mobile No.")
	#     elif ((self.phone and (re.match(self.regex_number, str(self.phone)) == None)) or 
	#         (self.phone and len(self.phone) != 10)):
	#         raise UserError("Enter valid 10 digits Parent Mobile No.")
	
	# @api.onchange('mobile')
	# def _onchange_mobile(self):
	#     if self.mobile and not str(self.mobile).isnumeric():
	#         return {
	#                 'warning': {
	#                     'title': _('Type Error'),
	#                     'message': _("Please use only numbers in Mobile No.")
	#                             }
	#                 }
	#     elif ((self.mobile and (re.match(self.regex_number, str(self.mobile)) == None)) or 
	#         (self.mobile and len(self.mobile) != 10)):
	#         return {
	#                 'warning': {
	#                     'title': _('Validation Error'),
	#                     'message': _("Enter valid 10 digits Mobile No.")
	#                             }
	#                 }
	#     if self.mobile and len(self.mobile)== 10:
	#         existing_number_mobile = self.env['res.partner'].search([('mobile','=', self.mobile),
	#                                 ('phone','=', self.mobile)])
	#         if existing_number_mobile:
	#             return {
	#                 'warning': {
	#                     'title': _('Mobile No. Already exist'),
	#                     'message': _("Mobile No. Already exist, Please check.")
	#                 }
	#             }
		
	# @api.onchange('phone')
	# def _onchange_phone(self):
	#     if self.phone and not str(self.phone).isnumeric():
	#         return {
	#                 'warning': {
	#                     'title': _('Type Error'),
	#                     'message': _("Please use only numbers in Parent Mobile No.")
	#                             }
	#                 }
	#     elif ((self.phone and (re.match(self.regex_number, str(self.phone)) == None)) or 
	#         (self.phone and len(self.phone) != 10)):
	#         return {
	#                 'warning': {
	#                     'title': _('Validation Error'),
	#                     'message': _("Enter valid 10 digits Parent Mobile No.")
	#                             }
	#                 }
	#     if self.phone and len(self.phone)== 10:
	#         existing_number_phone = self.env['res.partner'].search([('mobile','=', self.phone),
	#                                 ('phone','=', self.phone)])
	#         if existing_number_phone:
	#             return {
	#                 'warning': {
	#                     'title': _('Parent Mobile No. Already exist'),
	#                     'message': _("Parent Mobile No. Already exist, Please check.")
	#                 }
	#             }
	#         if self.mobile == self.phone:
	#             return {
	#                 'warning': {
	#                     'title': _('Same Mobile No. and Parent Mobile No.'),
	#                     'message': _("Mobile No. and Parent Mobile No. are same")
	#                 }
	#             }

	@api.constrains('email')
	def _validate_email(self):
		if self.email and (re.match(self.regex_mail, self.email) == None):
			raise UserError("Enter valid Email")
	
	@api.onchange('email')
	def _onchange_email(self):
		if self.email and (re.match(self.regex_mail, self.email) == None):
			return {
					'warning': {
						'title': _('Validation Error'),
						'message': _("Enter valid Email")
								}
					}
	
	def approve_student_action(self):
		student_ids=self.env['res.partner'].browse(self.ids)
		for student_id in student_ids:
			if student_id.states == 'in_progress' or student_id.states == 'on_hold':
				student_id.states='approved'