# -*- coding: utf-8 -*-

from odoo import models, fields

class ResStudent(models.Model):
    _inherit = 'account.move'
    _description = 'Account move inherit student'
    _rec_name = "name"

    s_covered = fields.Many2one('amb.batch.sectors', string='Sector', help="Sector Covered")
    course_id = fields.Many2one('amb.courses', string='Course', help="Job Role Course Trade")
    batch_name_id = fields.Many2one('amb.batch', string='Batch')
    fee_paid_by = fields.Selection([
        ('self', 'Self'),
        ('other', 'Other')
    ], string='Fee Paid By',default='self')
    source_funding = fields.Char(string='Source of Funding')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')