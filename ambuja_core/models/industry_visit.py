# -*- coding: utf-8 -*-

from odoo import models, fields, api


class IndustryVisit(models.Model):
    _name = 'amb.industry.visit'
    _inherit = "mail.thread"
    _description = 'Industry Visit Information'
    _rec_name = "company_name_id"

    visit_date = fields.Date(string='Visit Date', default=fields.Date.today(), required=True)
    previous_support = fields.Char(string='Previous Support If Any')
    fillowup_date = fields.Date(string='Followup Date')
    remarks = fields.Text(string='Remarks')
    street = fields.Char(related='company_name_id.street')
    street2 = fields.Char(related='company_name_id.street2')
    zip = fields.Char(related='company_name_id.zip', change_default=True)
    city = fields.Char(related='company_name_id.city')
    state_id = fields.Many2one("res.country.state",related='company_name_id.state_id', string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country',related='company_name_id.country_id', string='Country', ondelete='restrict')

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')

    #Relational Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    s_covered = fields.Many2one('amb.batch.sectors', string='Sector Covered', required=True)
    j_role = fields.Many2one('amb.courses', string='Job Role Course Trade', required=True)
    scope_of_partner = fields.Many2one('amb.scope.partnership', string='Scope of Partnership')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')

    #employer related fields
    
    company_name_id = fields.Many2one('res.partner', string='Company Name', required=True)
    contact_name_id = fields.Many2one('res.partner',string='Contact Person Name', required=True)
    contact_designation = fields.Char(related='contact_name_id.function', string='Contact Designation', required=True)
    contact_number = fields.Char(related='contact_name_id.mobile', string='Contact Number', size=10, required=True)
    contact_email = fields.Char(related='contact_name_id.email', string='Contact Mail', required=True)

    def student_reset(self):
        self.states='draft'
    
    def student_done(self):
        self.states='done'
