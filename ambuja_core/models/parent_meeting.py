# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ParentMeeting(models.Model):
    _name = 'amb.parent.meeting'
    _inherit = "mail.thread"
    _description = 'Parent Meeting'
    _rec_name = 'student_name_id'

    meeting_date = fields.Date(string='Meeting Date',
                            default=fields.Date.today(), required=True)
    parent_name = fields.Char(string='Parent Name', required=True)
    parent_contact_no = fields.Char(string='Parent Contact No.', size=10)
    parent_feedback = fields.Text(string='Parent Feedback', required=True)
    remarks = fields.Text(string='Remarks')
    street = fields.Char()
    street2 = fields.Char()
    zip = fields.Char(change_default=True)
    city = fields.Char()
    state_id = fields.Many2one("res.country.state", string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country', string='Country', ondelete='restrict', default = lambda self: self._get_default_country())

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')

    # Related Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    batch_name_id = fields.Many2one('amb.batch', string='Batch Name', required=True)
    student_name_id = fields.Many2one('res.partner', string='Student', required=True)
    course_id = fields.Many2one(related='student_name_id.course_id', string='Course')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
    b_e_date = fields.Date(string='Batch End Date', related='batch_name_id.b_e_date')
    tp_enrollment_no = fields.Char(string="Enrollment No.",related='student_name_id.tp_enrollment_no')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    def _get_default_country(self):
        country = self.env['res.country'].search([('code', '=', 'IN')], limit=1)
        return country

    def student_reset(self):
        self.states='draft'
    
    def student_done(self):
        self.states='done'