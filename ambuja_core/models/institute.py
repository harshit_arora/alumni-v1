# -*- coding: utf-8 -*-

from odoo import models, fields, api


class ResInstitute(models.Model):
    _name = "res.company"
    _inherit = ['res.company','mail.thread']
    _description = 'Institute'
    _rec_name = 'name'
    
    is_institute = fields.Boolean(string='Is Institute')
    district_code = fields.Char(string='District Code')

    states = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress','In Progress'),
        ('approved','Approved')
    ], string='Status',default='draft')
    
    @api.onchange('name')
    def on_change_name(self):
        country = self.env['res.country'].search([('code', '=', 'IN')], limit=1)
        self.country_id=country

    def institute_inprogress(self):
        self.states='in_progress'
        self.is_institute = True
    
    def institute_approved(self):
        self.states='approved'

    def institute_reset(self):
        self.states='draft'
        self.is_institute = False

