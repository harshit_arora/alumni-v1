# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PlacementSupport(models.Model):
    _name = 'amb.placement.data'
    _inherit = "mail.thread"
    _description = 'Student Placement Data'
    _rec_name = 'student_id'

    selection_date = fields.Date(string='Selection Date',
                                default=fields.Date.today(), required=True)
    placement_status = fields.Selection([
        ('y', 'Yes'),
        ('n', 'No')
    ], string='Placement Status', default='y')
    employment_type = fields.Selection([
        ('self', 'Self Employed'),
        ('partner', 'Employed Through Partner'),
        ('higher_studies','Opted for Higher Studies')
    ], string='Employment Status', default='partner')
    apprentice = fields.Selection([
        ('y', 'Yes'),
        ('n', 'No')
    ], string='Apprenticeship', default='n')
    undertaking_of_self = fields.Selection([
        ('y', 'Yes'),
        ('n', 'No')
    ], string='Undertaking Of Self Employment', default='n')
    proof_upskill = fields.Selection([
        ('y', 'Yes'),
        ('n', 'No')
    ], string='Proof of Up Skilling', default='n')
    type_of_proof = fields.Char(string='Type of Proof')
    date_of_joining = fields.Date(string='Date of Joining')
    feedback_employer = fields.Char(string='Feedback')
    feedback_frequency = fields.Selection([
        ('monthly', 'Monthly'),
        ('two_months','Every 2 Months'),
        ('four_months','Every 4 Months'),
        ('half_yearly','Half-Yearly'),
        ('quarterly', 'Quarterly'),
        ('yearly','Yearly')
    ], string='Frequency Of Feedback')
    monthly_earning_before = fields.Integer(
        string='Monthly Earning Before Training')
    monthly_earning_current = fields.Integer(
        string='Current Monthly Earning ')
    remarks = fields.Text(string='Remarks')

    states = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress','In Progress'),
        ('done','Done'),
    ], string='States',default='draft')
    data_for_month = fields.Char(string='Data submit for Month')
    data_for_year= fields.Char(string='Data submit for Year')
    # Related Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    batch_name_id = fields.Many2one('amb.batch', string='Batch', required=True)
    student_id = fields.Many2one('res.partner', string='Student', required=True)
    course_id = fields.Many2one(related='student_id.course_id', string='Course')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
    b_e_date = fields.Date(string='Batch End Date', related='batch_name_id.b_e_date')
    tp_enrollment_no = fields.Char(string="Enrollment No.",related='student_id.tp_enrollment_no')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')
    street = fields.Char(related='company_name_id.street')
    street2 = fields.Char(related='company_name_id.street2')
    zip = fields.Char(related='company_name_id.zip', change_default=True)
    city = fields.Char(related='company_name_id.city')
    state_id = fields.Many2one("res.country.state",related='company_name_id.state_id', string='State', ondelete='restrict', domain="[('country_id', '=?', country_id)]")
    country_id = fields.Many2one('res.country',related='company_name_id.country_id', string='Country', ondelete='restrict')

    #employer related fields
    
    company_name_id = fields.Many2one('res.partner', string='Company Name')
    contact_name_id = fields.Many2one('res.partner',string='Contact Person Name')
    contact_designation = fields.Char(related='contact_name_id.function', string='Contact Designation')
    contact_number = fields.Char(related='contact_name_id.mobile', string='Contact Number', size=10)
    contact_email = fields.Char(related='contact_name_id.email', string='Contact Mail')

    @api.onchange('employment_type')
    def on_change_employment_type(self):
        if (self.employment_type == 'self'):
            self.undertaking_of_self = 'y'
        else:
            self.undertaking_of_self = ''

    def student_reset(self):
        self.states='draft'
    
    def student_inprogress(self):
        self.states='in_progress'
    
    def student_done(self):
        self.states='done'
