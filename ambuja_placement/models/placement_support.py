# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PlacementSupport(models.Model):
    _name = 'amb.placement.support'
    _inherit = "mail.thread"
    _description = 'Student Placement Support'
    _rec_name = 'student_id'

    opportunity_date = fields.Date(string='Opportunity Date',
                                    default=fields.Date.today(), required=True)
    selected = fields.Selection([
        ('y', 'Yes'),
        ('n', 'No')
    ], string='Selected', default='n', required=True)
    remarks = fields.Text(string='Remarks')
    states = fields.Selection([
        ('draft', 'Draft'),
        ('in_progress','In Progress'),
        ('done','Done'),
    ], string='States',default='draft')

    # Related Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    batch_name_id = fields.Many2one('amb.batch', string='Batch', required=True)
    student_id = fields.Many2one('res.partner', string='Student', required=True)
    course_id = fields.Many2one(related='student_id.course_id', string='Course')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
    b_e_date = fields.Date(string='Batch End Date', related='batch_name_id.b_e_date')
    tp_enrollment_no = fields.Char(string="Enrollment No.",related='student_id.tp_enrollment_no')
    company_name_id = fields.Many2one('res.partner', string='Company Name', required=True)
    contact_name_id = fields.Many2one('res.partner',string='Contact Person Name', required=True)
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    def student_reset(self):
        self.states='draft'
    
    def student_inprogress(self):
        self.states='in_progress'
    
    def student_done(self):
        self.states='done'
        if(self.selected == 'y'):
            student_obj = self.env['amb.placement.data']
            student_obj.create({
                    'institute_id' : self.institute_id.id,
                    'batch_name_id' : self.batch_name_id.id,
                    'company_name_id' : self.company_name_id.id,
                    'contact_name_id' : self.contact_name_id.id,
                    'student_id' : self.student_id.id,
                    'course_id' : self.course_id.id,
                    'selection_date' : self.opportunity_date,
                    'placement_status' : self.selected,
                })

    def view_placement_data(self):
        """
        This function returns Tree view of student's placement data.
        """
        return {
        'name': (self.student_id.name),
        'type': 'ir.actions.act_window',
        'res_model': 'amb.placement.data',
        'view_mode': 'tree,form',
        'domain': [('student_id', '=', self.student_id.id)],
        }