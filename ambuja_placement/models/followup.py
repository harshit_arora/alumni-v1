# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Followup(models.Model):
    _name = 'amb.followup'
    _inherit = "mail.thread"
    _description = 'Student Followup'
    _rec_name = 'student_id'

    name = fields.Char(string='Name')
    followup_date = fields.Date(string='Followup Date')
    job_position = fields.Char(string='Job Position')
    contacted = fields.Boolean(string='Contacted')
    placed = fields.Boolean(string='Placed')
    notplaced_reason = fields.Char(string='Not Placed Reason')
    joining_date = fields.Date(string='Joining Date')
    monthly_income = fields.Integer(string='Monthly Income')
    employment_type = fields.Selection([
        ('w', 'Wage'),
        ('s', 'Self')
    ], string='ACF Employment Type', default='w')
    details = fields.Char(string='Details')

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')
    
    # Related Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    batch_name_id = fields.Many2one('amb.batch', string='Batch', required=True)
    student_id = fields.Many2one('res.partner', string='Student', required=True)
    course_id = fields.Many2one(related='student_id.course_id', string='Course')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
    b_e_date = fields.Date(string='Batch End Date', related='batch_name_id.b_e_date')
    tp_enrollment_no = fields.Char(string="Enrollment No.",related='student_id.tp_enrollment_no')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    def student_reset(self):
        self.states='draft'
    
    def student_done(self):
        self.states='done'
