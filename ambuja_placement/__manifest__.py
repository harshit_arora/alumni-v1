# -*- coding: utf-8 -*-
{
    'name': "Placements",
    'sequence': 3,
    'summary': """
        Placement Menu""",

    'description': """
        This module is developed for placement data of students.
    """,

    'author': "Modifyed",
    'website': "",
    'category': 'Generic Module',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','ambuja_core','mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/placement_support_view.xml',
        'views/placement_data_view.xml',
        'views/followup_view.xml',
    ],
}
