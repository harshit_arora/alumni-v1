# -*- coding: utf-8 -*-
# !/usr/bin/python


from odoo import api, fields, models, _
from odoo.exceptions import UserError
from io import StringIO
from io import BytesIO
import datetime
import xlrd
import base64
import urllib
import logging
import re
logger = logging.getLogger(__name__)
import datetime
from dateutil.relativedelta import relativedelta

# class DataWiz(models.TransientModel):
#     _name = 'data.wiz'

#     text_message = fields.Text('Message')

class ImportDataXLXInherit(models.Model):
    '''
      This class to provide wizard to upload xlx file
    '''
    _name = 'import.ambuja.data'

    file_path = fields.Char(string='File Path', size=256)
    xls_file = fields.Binary(attachment=True, string='XLS File')
    filename = fields.Char()



    def read_xlx_file(self):
        '''
         cursereate Temp xlx file
        '''
        file_data = self.xls_file
        if not file_data:
            raise UserError(_('Error!', "Please Select a File"))
        else:
            val = base64.decodestring(file_data)
            tempfile = BytesIO()
            tempfile.write(val)
            work_book = xlrd.open_workbook(file_contents=tempfile.getvalue())
        return work_book


    # import enquiry script

    def import_enquiry(self):
        '''
          This function is to read data from xls.
        '''
        work_book = self.read_xlx_file()
        sheet_list = work_book.sheet_names()
        if 'enquiry_data_format' in sheet_list:
            sheet = work_book.sheet_by_name('enquiry_data_format')
        else:
            raise UserError(_("There is no sheet of name 'enquiry_data_format',\
            Please make sure your Excel file having sheet with Name 'enquiry_data_format'."))
        n_rows = sheet.nrows

        for row in range(1, n_rows):
            enquiry = self.env['amb.enquiry'].search([('mobile', '=', str(sheet.row_values(row)[7]).strip())])
            if not enquiry:



                gender = (str(sheet.row_values(row)[5]).strip()).lower()
                if gender == 'male':
                    gender = 'm'
                if gender == 'female':
                    gender ='f'
                if gender == 'other':
                    gender = 'o'

                employment_status = (str(sheet.row_values(row)[23]).strip()).lower()
                if employment_status == 'unemployed':
                    employment_status = 'unemployed'
                if employment_status == 'self employed':
                    employment_status ='self'
                if employment_status == 'job':
                    employment_status = 'Job'


                education_status = (str(sheet.row_values(row)[24]).strip()).lower()
                if education_status == 'Pursuing':
                    education_status = 'pursuing'
                if education_status == 'Pass Out':
                    education_status ='pass'
                if education_status == 'Drop Out':
                    education_status = 'Drop'


                mobilized_activity = (str(sheet.row_values(row)[25]).strip()).lower()
                if mobilized_activity == 'mouth publicity':
                    mobilized_activity = 'mouth'
                if mobilized_activity == 'door to door mobilization':
                    mobilized_activity ='d2d'
                if mobilized_activity == 'community meeting':
                    mobilized_activity = 'community'
                if mobilized_activity == 'advertisement':
                    mobilized_activity = 'ad'
                if mobilized_activity == 'friend/referral':
                    mobilized_activity = 'referral'

                cycromatic_test = (str(sheet.row_values(row)[26]).strip()).lower()
                cycromatic_test_value = ''
                if cycromatic_test == 'yes':
                    cycromatic_test = True
                    cycromatic_test_value = str(sheet.row_values(row)[27]).strip()
                else:
                    cycromatic_test = False

                zip = sheet.row_values(row)[20]
                if type(zip) == float:
                    i = re.split(r'[.]', str(zip))
                    zip = i[0]
                else:
                    zip = str(sheet.row_values(row)[20]).strip()

                dob = sheet.row_values(row)[6]
                if type(dob) == float:
                    e_dob = datetime.datetime(*xlrd.xldate_as_tuple(dob, work_book.datemode))
                elif type(dob) == str:
                    e_dob = datetime.datetime.strptime(str(dob),'%m-%d-%Y')
                
                enquiry_date = sheet.row_values(row)[15]
                if type(enquiry_date) == float:
                    e_date = datetime.datetime(*xlrd.xldate_as_tuple(enquiry_date, work_book.datemode))
                elif type(enquiry_date) == str:
                    e_date = datetime.datetime.strptime(str(enquiry_date),'%m-%d-%Y')

                salutation_id = self.env['amb.student.salutation'].search([('name', '=', str(sheet.row_values(row)[0]).strip())],order="id asc",limit=1)
                if not salutation_id:
                    raise UserError(_("Line no %s not created yet, Please first create Salutation with name %s and try again.") %(row+1, str(sheet.row_values(row)[0]).strip()))
                else:
                    name = salutation_id.name + ' ' + str(sheet.row_values(row)[1]).strip() + ' ' + str(sheet.row_values(row)[2]).strip() + ' ' + str(sheet.row_values(row)[3]).strip()

                institute_id = self.env['res.company'].search([('name', '=', str(sheet.row_values(row)[12]).strip())],order="id asc",limit=1)
                if not institute_id:
                    raise UserError(_("Line no %s not created yet, Please first create Institute with name %s and try again.") %(row+1, str(sheet.row_values(row)[12]).strip())) 

                batch_sectors_id = self.env['amb.batch.sectors'].search([('name', '=', str(sheet.row_values(row)[13]).strip())],order="id asc",limit=1)
                if not batch_sectors_id:
                    raise UserError(_("Line no %s not created yet, Please first create sector with name %s and try again.") %(row+1, str(sheet.row_values(row)[13]).strip())) 
                                                    
                education_level_id = self.env['amb.student.education.level'].search([('name', '=', str(sheet.row_values(row)[14]).strip())],order="id asc",limit=1)
                if not education_level_id:
                    raise UserError(_("Line no %s not created yet, Please first create education level with name %s and try again.") %(row+1, str(sheet.row_values(row)[14]).strip()))

                course_id = self.env['amb.courses'].search([('name', '=', str(sheet.row_values(row)[16]).strip())],order="id asc",limit=1)
                if not course_id:
                    raise UserError(_("Line no %s not created yet, Please first create course with name %s and try again.") %(row+1, str(sheet.row_values(row)[16]).strip()))


                state_id = self.env['res.country.state'].search([('name', '=', str(sheet.row_values(row)[21]).strip())],order="id asc",limit=1)
                country_id = self.env['res.country'].search([('name', '=', str(sheet.row_values(row)[22]).strip())],order="id asc",limit=1)

                enquiry_id = self.env['amb.enquiry'].create({
                    'salutation_id': salutation_id.id,
                    'first_name': str(sheet.row_values(row)[1]).strip(),
                    'middle_name': str(sheet.row_values(row)[2]).strip(),
                    'last_name': str(sheet.row_values(row)[3]).strip(),
                    'name': name,
                    'gender': gender,
                    'dob': e_dob,
                    'mobile': str(sheet.row_values(row)[7]).strip(),
                    'email': str(sheet.row_values(row)[8]).strip(),
                    'aadhar_no': str(sheet.row_values(row)[9]).strip(),
                    'parent_name': str(sheet.row_values(row)[10]).strip(),
                    'mobile_parent': str(sheet.row_values(row)[11]).strip(),
                    'institute_id': institute_id.id,
                    's_covered': batch_sectors_id.id,
                    'edu_level' : education_level_id.id,
                    'enquiry_date': e_date,
                    'course_id': course_id.id,
                    'street': str(sheet.row_values(row)[17]).strip(),
                    'street2': str(sheet.row_values(row)[18]).strip(),
                    'city': str(sheet.row_values(row)[19]).strip(),
                    'zip': zip,
                    'state_id' : state_id.id,
                    'country_id' : country_id.id,
                    'employment_status' : employment_status,
                    'education_status': education_status,
                    'mobilized_activity': mobilized_activity,
                    'cycromatic_test': cycromatic_test,
                    'cycromatic_test_value': cycromatic_test_value,
                    'remarks': str(sheet.row_values(row)[28]).strip(),
                })
                self._cr.commit()

