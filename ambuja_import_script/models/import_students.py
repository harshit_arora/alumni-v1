# -*- coding: utf-8 -*-
# !/usr/bin/python


from odoo import api, fields, models, _
from odoo.exceptions import UserError
from io import StringIO
from io import BytesIO
import datetime
import xlrd
import base64
import urllib
import logging
import re
logger = logging.getLogger(__name__)
import datetime
from dateutil.relativedelta import relativedelta


class ImportDataXLXInherit(models.Model):
    '''
      This class to provide wizard to upload xlx file
    '''
    _inherit = 'import.ambuja.data'

    # import Students script

    def import_students(self):
        '''
          This function is to read data from xls.
        '''
        work_book = self.read_xlx_file()
        sheet_list = work_book.sheet_names()
        if 'student_data_format' in sheet_list:
            sheet = work_book.sheet_by_name('student_data_format')
        else:
            raise UserError(_("There is no sheet of name 'student_data_format',\
            Please make sure your Excel file having sheet with Name 'student_data_format'."))
        n_rows = sheet.nrows
        for row in range(1, n_rows):

            if type(sheet.row_values(row)[6]) == float:
                i = re.split(r'[.]', str(sheet.row_values(row)[6]))
                mobile = i[0]
            else:
                mobile = str(sheet.row_values(row)[6]).strip()

            student = self.env['res.partner'].search([('mobile', '=', mobile)])

            if not student:

                salutation_id = self.env['amb.student.salutation'].search([('name', '=', str(sheet.row_values(row)[0]).strip())], limit=1)
                if not salutation_id:
                    raise UserError(_("Line no %s not created yet, Please first create Salutation with name %s and try again.") %(row+1, str(sheet.row_values(row)[0]).strip()))
                else:
                    name = salutation_id.name + ' ' + str(sheet.row_values(row)[1]).strip() + ' ' + str(sheet.row_values(row)[2]).strip() + ' ' + str(sheet.row_values(row)[3]).strip()

                dob = sheet.row_values(row)[5]
                if type(dob) == float:
                    e_dob = datetime.datetime(*xlrd.xldate_as_tuple(dob, work_book.datemode))
                elif type(dob) == str:
                    e_dob = datetime.datetime.strptime(str(dob),'%m-%d-%Y')

                institute_id = self.env['res.company'].search([('name', '=', str(sheet.row_values(row)[8]).strip())],order="id asc",limit=1)
                if not institute_id:
                    raise UserError(_("Line no %s not created yet, Please first create Institute with name %s and try again.") %(row+1, str(sheet.row_values(row)[8]).strip()))

                course_id = self.env['amb.courses'].search([('name', '=', str(sheet.row_values(row)[9]).strip())],order="id asc",limit=1)
                if not course_id:
                    raise UserError(_("Line no %s not created yet, Please first create course with name %s and try again.") %(row+1, str(sheet.row_values(row)[9]).strip()))


                batch_id = self.env['amb.batch'].search([('name', '=', str(sheet.row_values(row)[10]).strip())], limit=1)
                if not batch_id:
                    raise UserError(_("Line no %s not created yet, Please first create batch with name %s and try again.") %(row+1, str(sheet.row_values(row)[10]).strip()))

                phone = sheet.row_values(row)[13]
                if type(phone) == float:
                    i = re.split(r'[.]', str(phone))
                    phone = i[0]
                else:
                    phone = str(sheet.row_values(row)[13]).strip()
                
                below_pov_line = (str(sheet.row_values(row)[14]).strip()).lower()
                if below_pov_line == 'yes':
                    below_pov_line = 'y'
                if below_pov_line == 'no':
                    below_pov_line = 'n'

                zip = sheet.row_values(row)[18]
                if type(zip) == float:
                    i = re.split(r'[.]', str(zip))
                    zip = i[0]
                else:
                    zip = str(sheet.row_values(row)[18]).strip()

                state_id = self.env['res.country.state'].search([('name', '=', str(sheet.row_values(row)[19]).strip())],order="id asc",limit=1)
                country_id = self.env['res.country'].search([('name', '=', str(sheet.row_values(row)[20]).strip())],order="id asc",limit=1)
                
                gender = (str(sheet.row_values(row)[21]).strip()).lower()
                if gender == 'male':
                    gender = 'm'
                if gender == 'female':
                    gender ='f'
                if gender == 'other':
                    gender = 'o'

                education_level_id = self.env['amb.student.education.level'].search([('name', '=', str(sheet.row_values(row)[22]).strip())],order="id asc",limit=1)
                if not education_level_id:
                    raise UserError(_("Line no %s not created yet, Please first create Education Leval with name %s and try again.") %(row+1, str(sheet.row_values(row)[22]).strip()))

                maritial_status = (str(sheet.row_values(row)[23]).strip()).lower()
                if maritial_status == 'married':
                    maritial_status = 'm'
                if maritial_status == 'single':
                    maritial_status = 's'

                religion_id = self.env['amb.student.religion'].search([('name', '=', str(sheet.row_values(row)[24]).strip())],order="id asc",limit=1)
                if not religion_id:
                    raise UserError(_("Line no %s not created yet, Please first create Religion with name %s and try again.") %(row+1, str(sheet.row_values(row)[24]).strip())) 

                caste = (str(sheet.row_values(row)[26]).strip()).lower()
                if caste == 'sc':
                    caste = 'sc'
                if caste == 'st':
                    caste = 'st'
                if caste == 'obc':
                    caste = 'obc'
                if caste == 'gen':
                    caste = 'gen'

                aadhar_no = sheet.row_values(row)[27]
                if type(aadhar_no) == float:
                    i = re.split(r'[.]', str(aadhar_no))
                    aadhar_no = i[0]
                else:
                    aadhar_no = str(sheet.row_values(row)[27]).strip()

                pass_no = sheet.row_values(row)[28]
                if type(pass_no) == float:
                    i = re.split(r'[.]', str(pass_no))
                    pass_no = i[0]
                else:
                    pass_no = str(sheet.row_values(row)[28]).strip()

                val_date = sheet.row_values(row)[29]
                if type(val_date) == float:
                    val_date = datetime.datetime(*xlrd.xldate_as_tuple(val_date, work_book.datemode))
                elif type(val_date) == str:
                    val_date = datetime.datetime.strptime(str(val_date),'%m-%d-%Y')

                r_no = sheet.row_values(row)[30]
                if type(r_no) == float:
                    i = re.split(r'[.]', str(r_no))
                    r_no = i[0]
                else:
                    r_no = str(sheet.row_values(row)[30]).strip()

                annual_h_income_id = self.env['amb.student.annual.income'].search([('name', '=', str(sheet.row_values(row)[31]).strip())],order="id asc",limit=1)
                if not annual_h_income_id:
                    raise UserError(_("Line no %s not created yet, Please first create Annual Household Income with name %s and try again.") %(row+1, str(sheet.row_values(row)[31]).strip()))

                fam_ocp = (str(sheet.row_values(row)[32]).strip()).lower()
                if fam_ocp == 'agriculture/own farming':
                    fam_ocp = 'a/o'
                if fam_ocp == 'self employed':
                    fam_ocp = 'self'
                if fam_ocp == 'agriculture wages':
                    fam_ocp = 'aw'
                if fam_ocp == 'government job':
                    fam_ocp = 'gj'
                if fam_ocp == 'labour':
                    fam_ocp = 'l'
                if fam_ocp == 'private job':
                    fam_ocp = 'p'
                if fam_ocp == 'other':
                    fam_ocp = 'o'

                disability = (str(sheet.row_values(row)[35]).strip()).lower()
                if disability ==  'yes':
                    disability = True
                else:
                    disability = False

                acc_no = sheet.row_values(row)[36]
                if type(acc_no) == float:
                    i = re.split(r'[.]', str(acc_no))
                    acc_no = i[0]
                else:
                    acc_no = str(sheet.row_values(row)[36]).strip()

                tech_edu = ''
                if str(sheet.row_values(row)[38]).strip() == 'Yes':
                    tech_edu = 'y'
                if str(sheet.row_values(row)[38]).strip() == 'No':
                    tech_edu = 'n'

                t_mobile = (str(sheet.row_values(row)[39]).strip()).lower()
                if t_mobile == 'smart phone':
                    t_mobile = 's'
                if t_mobile == 'other':
                    t_mobile = 'o'  
                if t_mobile == 'none':
                    t_mobile = 'n' 

                y_of_pre_ex = sheet.row_values(row)[40]
                if type(y_of_pre_ex) == float:
                    i = re.split(r'[.]', str(y_of_pre_ex))
                    y_of_pre_ex = i[0]
                else:
                    y_of_pre_ex = str(sheet.row_values(row)[40]).strip()

                m_of_pre_ex = sheet.row_values(row)[41]
                if type(m_of_pre_ex) == float:
                    i = re.split(r'[.]', str(m_of_pre_ex))
                    m_of_pre_ex = i[0]
                else:
                    m_of_pre_ex = str(sheet.row_values(row)[41]).strip()

                skill_list = []
                s_g = str(sheet.row_values(row)[42]).strip()
                if s_g:
                    skill_g = re.split(r'[,]', str(s_g))
                    for skill in skill_g:
                        skill_id = self.env['amb.skill.group'].search([
                        ('name','=',skill.strip())])
                        if skill_id:
                            skill_list.append(skill_id.id)
                        else:
                            raise UserError(_("Line no %s not created yet, Please first create Skill Group with name %s and try again.") %(row+1, skill.strip()))

                student_id = self.env['res.partner'].create({
                    'salutation_id': salutation_id.id,
                    'first_name': str(sheet.row_values(row)[1]).strip(),
                    'middle_name': str(sheet.row_values(row)[2]).strip(),
                    'last_name': str(sheet.row_values(row)[3]).strip(),
                    'name': name,
                    'dob': e_dob,
                    'mobile': mobile,
                    'email': str(sheet.row_values(row)[7]).strip(),
                    'institute_id': institute_id.id,
                    'course_id': course_id.id,
                    'batch_name_id': batch_id.id,
                    'father_name': str(sheet.row_values(row)[11]).strip(),
                    'mother_name': str(sheet.row_values(row)[12]).strip(),
                    'phone': phone,
                    'below_pov_line': below_pov_line,
                    'street': str(sheet.row_values(row)[15]).strip(),
                    'street2': str(sheet.row_values(row)[16]).strip(),
                    'city': str(sheet.row_values(row)[17]).strip(),
                    'zip': zip,
                    'state_id' : state_id.id,
                    'country_id' : country_id.id,
                    'gender': gender,
                    'edu_level' : education_level_id.id,
                    'maritial_status': maritial_status,
                    'religion': religion_id.id,
                    'place_birth': str(sheet.row_values(row)[25]).strip(),
                    'caste': caste,
                    'aadhar_no': aadhar_no,
                    'pass_no': pass_no,
                    'val_date': val_date,
                    'ration_no': r_no,
                    'annual_h_income': annual_h_income_id.id,
                    'fam_ocp': fam_ocp,
                    'pre_training_status': str(sheet.row_values(row)[33]).strip(),
                    'sdms_enrol_no': str(sheet.row_values(row)[34]).strip(),
                    'disability': disability,
                    'account_number': acc_no,
                    'ifsc_code': str(sheet.row_values(row)[37]).strip(),
                    'tech_edu': tech_edu,
                    't_mobile': t_mobile,
                    'y_of_pre_ex': y_of_pre_ex,
                    'm_of_pre_ex': m_of_pre_ex,
                    'skill_group_ids': [(6,0, skill_list)],
                    'remarks': str(sheet.row_values(row)[43]).strip(),
                    'is_company': False,
                    'function': False,
                    'is_student': True,
                })
                self._cr.commit()


