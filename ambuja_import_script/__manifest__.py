# -*- coding: utf-8 -*-
# Part of Odoo. See LICENSE file for full copyright and licensing details.

{
    'name': 'Ambuja Import Script',
    'category': 'Import Data ',
    'depends': ['ambuja_core'],
    'author': "Modifyed",
    'description': """
This is the module for Import all the data.
========================================================================""",
    'data': [
        'security/ir.model.access.csv',
        'views/import_enquiry.xml',
    ],
    'installable': True,
    'auto_install': False,
}
