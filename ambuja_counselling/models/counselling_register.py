# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Fees(models.Model):
    _name = 'amb.counselling.register'
    _inherit = "mail.thread"
    _description = 'Student Counselling Register'
    _rec_name = 'student_name_id'

    dt_date_of_counselling = fields.Date(string='DT Counselling Date')
    dt_issue_candidate = fields.Text(string='DT Issues Discussed With Candidates', required=True)
    dt_issue_parents = fields.Text(string='DT Issues Discussed With Parents')
    at_date_of_counselling = fields.Date(string='AT Counselling Date')
    at_issue_candidate = fields.Text(string='AT Issues Discussed With Candidates', required=True)
    at_issue_parents = fields.Text(string='AT Issues Discussed With Parents')
    remarks = fields.Text(string='Remarks')

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')

    # Related Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    batch_name_id = fields.Many2one('amb.batch', string='Batch', required=True)
    student_name_id = fields.Many2one('res.partner', string='Student', required=True)
    course_id = fields.Many2one(related='student_name_id.course_id', string='Course')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    b_s_date = fields.Date(string='Batch Start Date', related='batch_name_id.b_s_date')
    b_e_date = fields.Date(string='Batch End Date', related='batch_name_id.b_e_date')
    tp_enrollment_no = fields.Char(string="Enrollment No.",related='student_name_id.tp_enrollment_no')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    def student_reset(self):
        self.states='draft'
    
    def student_done(self):
        self.states='done'