# -*- coding: utf-8 -*-

from odoo import models, fields, api


class Fees(models.Model):
    _name = 'amb.pre.counselling.register'
    _inherit = "mail.thread"
    _description = 'Student Pre Counselling Register'
    _rec_name = 'batch_name_id'

    counselling_date = fields.Date(string='Counselling Date', required=True)
    issue_candidate = fields.Text(string='Issues Discussed With Candidates', required=True)
    remarks = fields.Text(string='Remarks')

    states = fields.Selection([
        ('draft', 'Draft'),
        ('done','Done'),
    ], string='States',default='draft')

    # Related Fields
    institute_id = fields.Many2one('res.company', string='Institute')
    batch_name_id = fields.Many2one('amb.batch', string='Batch', required=True)
    student_name_ids = fields.Many2many('res.partner', string='Students', required=True)
    course_id = fields.Many2one(related='batch_name_id.course_id', string='Job Role Course Trade')
    s_covered = fields.Many2one(related='batch_name_id.s_covered', string='Sector Covered')
    fyear_id = fields.Many2one('amb.fiscal.year' ,string='FYear')


    def student_reset(self):
        self.states='draft'
    
    def student_done(self):
        self.states='done'