# -*- coding: utf-8 -*-
{
    'name': "Student Counselling",
    'sequence': 2,
    'summary': """
        Alumni Portal Counselling Menu""",

    'description': """
        This module is developed for councelling data of students.
    """,

    'author': "Modifyed",
    'website': "",
    'category': 'Generic Module',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base','ambuja_core','mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/pre_counselling_register_view.xml',
        'views/counselling_register_view.xml',
    ],
}
